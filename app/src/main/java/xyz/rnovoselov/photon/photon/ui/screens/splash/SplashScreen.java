package xyz.rnovoselov.photon.photon.ui.screens.splash;

import android.os.Bundle;
import android.util.Log;

import com.squareup.leakcanary.RefWatcher;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.List;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.data.errors.NetworkSocketError;
import xyz.rnovoselov.photon.photon.data.network.responces.PhotocardRes;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardTagRealm;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.di.scopes.DaggerScope;
import xyz.rnovoselov.photon.photon.flow.AbstractScreen;
import xyz.rnovoselov.photon.photon.flow.Screen;
import xyz.rnovoselov.photon.photon.mvp.models.SplashModel;
import xyz.rnovoselov.photon.photon.mvp.presenters.AbstractPresenter;
import xyz.rnovoselov.photon.photon.mvp.presenters.ISplashPresenter;
import xyz.rnovoselov.photon.photon.ui.activities.RootActivity;
import xyz.rnovoselov.photon.photon.ui.screens.raiting_photos_list.RatingPhotoCardsScreen;

/**
 * Created by roman on 05.06.17.
 */

@Screen(R.layout.screen_splash)
public class SplashScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int splashViewState = SplashView.NORMAL_STATE;

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerSplashScreen_Component.builder()
                .module(new Module())
                .rootComponent(parentComponent)
                .build();
    }

    public int getSplashViewState() {
        return splashViewState;
    }

    public void setSplashViewState(int splashViewState) {
        this.splashViewState = splashViewState;
    }

    //region =================== DI ====================

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(SplashScreen.class)
        SplashPresenter providePresenter() {
            return new SplashPresenter();
        }

        @Provides
        @DaggerScope(SplashScreen.class)
        SplashModel provideModel() {
            return new SplashModel();
        }

    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @DaggerScope(SplashScreen.class)
    public interface Component {
        void inject(SplashPresenter presenter);

        void inject(SplashView view);
    }

    //endregion

    //region ================ Presenter ================

    public class SplashPresenter extends AbstractPresenter<SplashView, SplashModel> implements ISplashPresenter {

        @Inject
        RefWatcher refWatcher;

        Subscription ratingPhotosSubs;
        Subscription tagsSubs;

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            Log.e(TAG, "onEnterScope");
            if (getView() != null) {
                if (getView().getViewState() == SplashView.NORMAL_STATE) {
                    Observable.mergeDelayError(model.updatePhotoCardsTags(), model.updatePhotoCardsList())
                            .subscribe(new Subscriber() {
                                @Override
                                public void onCompleted() {
                                    goToRaitingScreen();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    goToRaitingScreen();
                                    if (getRootView() != null && (e instanceof SocketTimeoutException || e instanceof ConnectException)) {
                                        getRootView().showError(new NetworkSocketError());
                                    }
                                }

                                @Override
                                public void onNext(Object o) {

                                }
                            });
//                    downloadBestRatingPhotos();
//                    downloadTags();
//                    downloadUserAlbums();
                }
                showProgress();
                getView().setViewState(SplashView.DOWNLOAD_STATE);
            }
        }

        @Override
        protected void onExitScope() {
            super.onExitScope();
            refWatcher.watch(this);
        }

        @Override
        protected void initActionBar() {
            rootPresenter.createActionbarBuilder()
                    .setVisible(false)
                    .build();

        }

        @Override
        protected void initBottomBar() {
            rootPresenter.showBottomBar(false);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            Log.e(TAG, "onLoad()");
            if (getView() != null && getView().getViewState() == SplashView.DOWNLOAD_STATE) {
                showProgress();
            }
        }

        //region ================ ISplashPresenter ================

        @Override
        public void downloadBestRatingPhotos() {

        }

        @Override
        public void downloadTags() {

        }

        @Override
        public void downloadUserAlbums() {

        }

        @Override
        public void showProgress() {
            if (getRootView() != null) {
                getRootView().showProgress(true);
            }
        }

        @Override
        public void hideProgress() {
            if (getRootView() != null) {
                getRootView().showProgress(false);
            }
        }

        @Override
        public void goToRaitingScreen() {
            hideProgress();
            Flow.get(getView()).set(new RatingPhotoCardsScreen());
        }

        //endregion
    }

//endregion
}
