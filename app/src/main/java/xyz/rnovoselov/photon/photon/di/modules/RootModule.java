package xyz.rnovoselov.photon.photon.di.modules;

import dagger.Module;
import dagger.Provides;
import xyz.rnovoselov.photon.photon.di.scopes.RootScope;
import xyz.rnovoselov.photon.photon.mvp.models.AccountModel;
import xyz.rnovoselov.photon.photon.mvp.presenters.RootPresenter;

/**
 * Created by roman on 02.06.17.
 */

@Module
public class RootModule {

    @Provides
    @RootScope
    public RootPresenter provideRootPresenter() {
        return new RootPresenter();
    }

    @Provides
    @RootScope
    public AccountModel provideAccountModel() {
        return new AccountModel();
    }
}
