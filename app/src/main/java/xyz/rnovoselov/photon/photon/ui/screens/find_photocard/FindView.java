package xyz.rnovoselov.photon.photon.ui.screens.find_photocard;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import butterknife.BindView;
import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.mvp.views.AbstractView;

/**
 * Created by roman on 18.06.17.
 */

public class FindView extends AbstractView<FindScreen.FindPresenter> {

    @BindView(R.id.find_pager)
    ViewPager pager;

    public FindView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<FindScreen.Component>getDaggerComponent(context).inject(this);
    }

    public ViewPager getViewPager() {
        return pager;
    }

    public void initView() {
        FindAdapter findAdapter = new FindAdapter();
        pager.setAdapter(findAdapter);
    }
}
