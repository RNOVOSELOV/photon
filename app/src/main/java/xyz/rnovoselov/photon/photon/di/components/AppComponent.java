package xyz.rnovoselov.photon.photon.di.components;

import android.content.Context;

import com.squareup.leakcanary.RefWatcher;

import dagger.Component;
import xyz.rnovoselov.photon.photon.di.modules.AppModule;

/**
 * Created by roman on 02.06.17.
 */

@Component(modules = AppModule.class)
public interface AppComponent {

    Context getContext();

    RefWatcher getCanaryWatcher();
}
