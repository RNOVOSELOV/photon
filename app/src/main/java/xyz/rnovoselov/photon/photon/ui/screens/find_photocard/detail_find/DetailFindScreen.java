package xyz.rnovoselov.photon.photon.ui.screens.find_photocard.detail_find;

import dagger.Provides;
import mortar.MortarScope;
import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.di.scopes.DaggerScope;
import xyz.rnovoselov.photon.photon.flow.AbstractScreen;
import xyz.rnovoselov.photon.photon.flow.Screen;
import xyz.rnovoselov.photon.photon.mvp.models.RatingPhotoCardModel;
import xyz.rnovoselov.photon.photon.mvp.presenters.AbstractPresenter;
import xyz.rnovoselov.photon.photon.ui.screens.find_photocard.FindScreen;

/**
 * Created by roman on 05.07.17.
 */

@Screen(R.layout.screen_find)
public class DetailFindScreen extends AbstractScreen<FindScreen.Component> {

    @Override
    public Object createScreenComponent(FindScreen.Component parentComponent) {
        return DaggerDetailFindScreen_Component.builder()
                .module(new Module())
                .component(parentComponent)
                .build();
    }

    //region =================== DI ====================

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(DetailFindScreen.class)
        DetailFindPresenter providesDetailFindPresenter() {
            return new DetailFindPresenter();
        }

    }

    @dagger.Component(dependencies = FindScreen.Component.class, modules = Module.class)
    @DaggerScope(DetailFindScreen.class)
    public interface Component {
        void inject(DetailFindPresenter presenter);

        void inject(DetailFindView view);

        RatingPhotoCardModel getRatingPhotoCardModel();
    }

    //endregion

    public class DetailFindPresenter extends AbstractPresenter<DetailFindView, RatingPhotoCardModel> {

        @Override
        protected void initActionBar() {

        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initBottomBar() {

        }

        public void searchPhotos(String searchString) {
            if (searchString.trim().length() != 0) {
                model.addStringToSearchHistory(searchString);
            }
        }
    }
}
