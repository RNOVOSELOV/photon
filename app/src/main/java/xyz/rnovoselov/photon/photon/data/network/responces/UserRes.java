package xyz.rnovoselov.photon.photon.data.network.responces;

import java.util.List;

/**
 * Created by novoselov on 16.06.2017.
 */

public class UserRes {
    private String name;
    private String login;
    private List<AlbumRes> albums = null;
    private String avatar;
    private int albumCount;
    private int photocardCount;

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public List<AlbumRes> getAlbums() {
        return albums;
    }

    public String getAvatar() {
        return avatar;
    }

    public int getAlbumCount() {
        return albumCount;
    }

    public int getPhotocardCount() {
        return photocardCount;
    }
}
