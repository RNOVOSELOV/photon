package xyz.rnovoselov.photon.photon.data.errors;

/**
 * Created by roman on 31.05.17.
 */

/**
 * Класс необходим для определения типа возникшей ошибки при отображении сообщения пользователю.
 * В продакшене ошибки приложения отображаются в {@link android.support.design.widget.Snackbar},
 * если они наследуются от этого класса, иначе отображается стандартное сообщение и
 * посылается отчет в коллектор ошибок.
 */
public abstract class AbstractPhotonError extends Throwable {

    AbstractPhotonError(String s) {
        super(s);
    }
}

