package xyz.rnovoselov.photon.photon.data.storage.realm;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import rx.Observable;
import xyz.rnovoselov.photon.photon.data.network.responces.PhotocardRes;

/**
 * Created by novoselov on 08.06.2017.
 */

public class PhotocardRealm extends RealmObject {

    @PrimaryKey
    private String id;
    private String title;
    private String photo;
    private String owner;
    private PhotocardFiltersRealm filters;
    private RealmList<PhotocardTagRealm> tags = new RealmList<>();
    private int favorits;
    private int views;
    private Date updated;
    private Date created;

    public PhotocardRealm() {
    }

    public PhotocardRealm(PhotocardRes response) {
        id = response.getId();
        title = response.getTitle();
        photo = response.getPhotoUrl();
        owner = response.getOwnerId();
        favorits = response.getFavorits();
        views = response.getViews();
        filters = new PhotocardFiltersRealm(response.getFilters());
        updated = response.getUpdated();
        created = response.getCreated();

        for (String tag : response.getTags()) {
            tags.add(new PhotocardTagRealm(tag));
        }
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPhoto() {
        return photo;
    }

    public String getOwner() {
        return owner;
    }

    public PhotocardFiltersRealm getFilters() {
        return filters;
    }

    public RealmList<PhotocardTagRealm> getTags() {
        return tags;
    }

    public int getFavorits() {
        return favorits;
    }

    public int getViews() {
        return views;
    }

    public Date getUpdated() {
        return updated;
    }

    public Date getCreated() {
        return created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhotocardRealm that = (PhotocardRealm) o;

        if (favorits != that.favorits) return false;
        if (views != that.views) return false;
        if (!id.equals(that.id)) return false;
        if (!title.equals(that.title)) return false;
        if (!photo.equals(that.photo)) return false;
        if (!owner.equals(that.owner)) return false;
        if (filters != null ? !filters.equals(that.filters) : that.filters != null) return false;
        return tags != null ? tags.equals(that.tags) : that.tags == null;

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + photo.hashCode();
        result = 31 * result + owner.hashCode();
        result = 31 * result + (filters != null ? filters.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + favorits;
        result = 31 * result + views;
        return result;
    }
}
