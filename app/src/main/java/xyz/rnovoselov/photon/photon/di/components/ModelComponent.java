package xyz.rnovoselov.photon.photon.di.components;

import javax.inject.Singleton;

import dagger.Component;
import xyz.rnovoselov.photon.photon.di.modules.ModelModule;
import xyz.rnovoselov.photon.photon.mvp.models.AbstractModel;

/**
 * Created by roman on 02.06.17.
 */

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
