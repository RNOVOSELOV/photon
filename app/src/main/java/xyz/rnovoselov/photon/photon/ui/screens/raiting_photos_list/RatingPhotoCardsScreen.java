package xyz.rnovoselov.photon.photon.ui.screens.raiting_photos_list;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;

import com.squareup.leakcanary.RefWatcher;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.data.providers.ResourceProvider;
import xyz.rnovoselov.photon.photon.data.storage.dto.PhotoCardDataDto;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardRealm;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.di.scopes.DaggerScope;
import xyz.rnovoselov.photon.photon.flow.AbstractScreen;
import xyz.rnovoselov.photon.photon.flow.Screen;
import xyz.rnovoselov.photon.photon.mvp.models.AccountModel;
import xyz.rnovoselov.photon.photon.mvp.models.RatingPhotoCardModel;
import xyz.rnovoselov.photon.photon.mvp.presenters.AbstractPresenter;
import xyz.rnovoselov.photon.photon.mvp.presenters.IRatingPhotocardPresenter;
import xyz.rnovoselov.photon.photon.mvp.presenters.MenuItemHolder;
import xyz.rnovoselov.photon.photon.mvp.presenters.RootPresenter;
import xyz.rnovoselov.photon.photon.ui.activities.RootActivity;
import xyz.rnovoselov.photon.photon.ui.screens.find_photocard.FindScreen;
import xyz.rnovoselov.photon.photon.ui.screens.photocard.PhotoCardScreen;

/**
 * Created by roman on 08.06.17.
 */

@Screen(R.layout.screen_raiting_photocards)
public class RatingPhotoCardsScreen extends AbstractScreen<RootActivity.RootComponent> {

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerRatingPhotoCardsScreen_Component.builder()
                .module(new Module())
                .rootComponent(parentComponent)
                .build();
    }

    //region ================ DI ================

    @dagger.Module
    public class Module {

        @Provides
        @DaggerScope(RatingPhotoCardsScreen.class)
        RatingPhotoCardModel providesRatingPhotoCardModel() {
            return new RatingPhotoCardModel();
        }

        @Provides
        @DaggerScope(RatingPhotoCardsScreen.class)
        RatingPhotoCardsPresenter providesRatingPresenter() {
            return new RatingPhotoCardsPresenter();
        }

        @Provides
        @DaggerScope(RatingPhotoCardsScreen.class)
        ResourceProvider provideResources(Context context) {
            return new ResourceProvider(context);
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @DaggerScope(RatingPhotoCardsScreen.class)
    public interface Component {
        void inject(RatingPhotoCardsPresenter presenter);

        void inject(RatingPhotoCardsView view);

        RootPresenter getRootPresenter();

        RefWatcher getRefWatcher();

        RatingPhotoCardModel getRatingPhotoCardModel();
    }

    //endregion

    //region ================ Presenter ================

    public class RatingPhotoCardsPresenter extends AbstractPresenter<RatingPhotoCardsView, RatingPhotoCardModel> implements IRatingPhotocardPresenter {

        @Inject
        RefWatcher refWatcher;

        @Inject
        ResourceProvider resourceProvider;

        @Inject
        AccountModel accountModel;

        int lastRecyclerPosition;

        MenuItem.OnMenuItemClickListener findListener = item -> {
            if (getRootView() != null) {
                Flow.get(getView()).set(new FindScreen());
            }
            return true;
        };

        MenuItem.OnMenuItemClickListener enterListener = item -> {
            if (getRootView() != null) {
                getRootView().showMessage("Enter");
            }
            return true;
        };

        MenuItem.OnMenuItemClickListener registerListener = item -> {
            if (getRootView() != null) {
                getRootView().showMessage("Reg");
            }
            return true;
        };

        @Override
        protected void initActionBar() {
            rootPresenter.createActionbarBuilder()
                    .setTitle(resourceProvider.getStringResource(R.string.app_name_splash_text))
                    .setBackArrow(false)
                    .setVisible(true)
                    .addAction(new MenuItemHolder("Поиск", R.drawable.ic_custom_search_black_24dp, findListener))
                    .addActionSubMenu(new MenuItemHolder("Войти", enterListener))
                    .addActionSubMenu(new MenuItemHolder("Зарегистрироваться", registerListener))
                    .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initBottomBar() {
            rootPresenter.showBottomBar(true);
        }

        @Override
        public void dropView(RatingPhotoCardsView view) {
            lastRecyclerPosition = getView().getCurrentRecyclerPosition();
            registerListener = null;
            findListener = null;
            enterListener = null;

            rootPresenter.clearTollBarMenu();
            super.dropView(view);
        }

        @Override
        protected void onExitScope() {
            refWatcher.watch(this);
            super.onExitScope();
        }

        public void processBackPressed() {
            if (getRootView() != null) {
                getRootView().closeApplication();
            }
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            compositeSubscription.add(ratingPhotoCardsSubscription());
        }

        //region ======================== IRatingPhotocardPresenter ========================

        @Override
        public void clickOnPhotoCard(PhotocardRealm photocardRealm) {
            Flow.get(getView()).set(new PhotoCardScreen(new PhotoCardDataDto(photocardRealm)));
        }

        //endregion

        private Subscription ratingPhotoCardsSubscription() {
            if (getRootView() != null) {
                //   getRootView().showProgress(true);
            }
            return model.getPhotoCards()
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe(new RealmSubscriber());
        }

        private class RealmSubscriber extends Subscriber<PhotocardRealm> {

            RatingPhotoCardsAdapter adapter = getView().getAdapter();

            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                if (getRootView() != null) {
                    getRootView().showError(e);
                }
            }

            @Override
            public void onNext(PhotocardRealm photocardRealm) {
                adapter.addPhotoCard(photocardRealm);
                //                getRootView().showProgress(false);
                getView().showRatingPhotosView();
            }
        }

    }

    //endregion

}
