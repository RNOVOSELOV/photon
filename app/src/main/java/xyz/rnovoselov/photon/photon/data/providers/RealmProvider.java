package xyz.rnovoselov.photon.photon.data.providers;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.Observable;
import xyz.rnovoselov.photon.photon.data.network.responces.PhotocardRes;
import xyz.rnovoselov.photon.photon.data.network.responces.UserRes;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardRealm;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardTagRealm;
import xyz.rnovoselov.photon.photon.data.storage.realm.UserRealm;

/**
 * Created by roman on 01.06.17.
 */

public class RealmProvider {

    private Realm realmInstance;

    public RealmProvider() {
    }

    public void saveTagsToRealm(List<PhotocardTagRealm> tags) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(realm1 -> realm1.insertOrUpdate(tags));
        realm.close();
    }

    public void savePhotocardResponseToRealm(PhotocardRes response) {
        if (!response.isActive()) {
            deleteFromRealm(PhotocardRealm.class, response.getId());
        }

        Realm realm = Realm.getDefaultInstance();
        PhotocardRealm photocardRealm = new PhotocardRealm(response);
        realm.executeTransactionAsync(realm1 -> realm1.insertOrUpdate(photocardRealm));
        realm.close();
    }

    public void saveUser(String id, UserRes userRes) {
        Realm realm = Realm.getDefaultInstance();
        UserRealm userRealm = new UserRealm(id, userRes);
        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(userRealm));
        realm.close();
    }

    public Observable<PhotocardRealm> getPhotoCards() {
        RealmResults<PhotocardRealm> managePhotoCards = getQueryRealmInstance()
                .where(PhotocardRealm.class)
                .findAllSortedAsync("views", Sort.DESCENDING, "title", Sort.ASCENDING);
        return managePhotoCards
                .asObservable()
                .filter(RealmResults::isLoaded)
                .flatMap(Observable::from);
    }

    public Observable<UserRealm> getUser(String userId) {
        RealmResults<UserRealm> manageUser = getQueryRealmInstance()
                .where(UserRealm.class)
                .equalTo("id", userId)
                .findAllAsync();
        return manageUser
                .asObservable()
                .filter(RealmResults::isLoaded)
                .first()
                .flatMap(Observable::from);
    }

    private <T extends RealmObject> Observable<T> convertRealmResultToRxJavaObservable(RealmResults<T> results) {
        return results
                .asObservable()                         // realm converts to RxJava v1 only
                .filter(RealmResults::isLoaded)
                .first()                                // hot observable to cold
                .flatMap(Observable::from);
    }

    public void deleteFromRealm(Class<? extends RealmObject> entityRealmClass, String id) {
        Realm realm = Realm.getDefaultInstance();
        RealmObject entity = realm.where(entityRealmClass).equalTo("id", id).findFirst();
        if (entity != null) {
            realm.executeTransaction(realm1 -> entity.deleteFromRealm());
        }
        realm.close();
    }

    private Realm getQueryRealmInstance() {
        if (realmInstance == null || realmInstance.isClosed()) {
            realmInstance = Realm.getDefaultInstance();
        }
        return realmInstance;
    }
}
