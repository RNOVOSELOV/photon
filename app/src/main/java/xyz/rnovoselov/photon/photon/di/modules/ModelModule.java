package xyz.rnovoselov.photon.photon.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import xyz.rnovoselov.photon.photon.data.providers.DataProvider;

/**
 * Created by roman on 02.06.17.
 */

@Module
public class ModelModule {

    @Provides
    @Singleton
    DataProvider provideDataProvider () {
        return DataProvider.getInstance();
    }
}
