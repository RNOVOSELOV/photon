package xyz.rnovoselov.photon.photon.data.providers;

import android.content.Context;

import java.net.ContentHandler;

/**
 * Created by novoselov on 08.06.2017.
 */

public class ResourceProvider {

    private Context context;

    public ResourceProvider(Context context) {
        this.context = context;
    }

    public String getStringResource(int resourceId) {
        return context.getString(resourceId);
    }
}
