package xyz.rnovoselov.photon.photon.ui.screens.find_photocard;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.squareup.leakcanary.RefWatcher;

import javax.inject.Inject;

import dagger.Provides;
import flow.TreeKey;
import mortar.MortarScope;
import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.di.scopes.DaggerScope;
import xyz.rnovoselov.photon.photon.flow.AbstractScreen;
import xyz.rnovoselov.photon.photon.flow.Screen;
import xyz.rnovoselov.photon.photon.mvp.models.RatingPhotoCardModel;
import xyz.rnovoselov.photon.photon.mvp.presenters.AbstractPresenter;
import xyz.rnovoselov.photon.photon.mvp.presenters.RootPresenter;
import xyz.rnovoselov.photon.photon.ui.screens.raiting_photos_list.RatingPhotoCardsScreen;

/**
 * Created by roman on 18.06.17.
 */

@Screen(R.layout.screen_find_cards)
public class FindScreen extends AbstractScreen<RatingPhotoCardsScreen.Component> implements TreeKey {

    @NonNull
    @Override
    public Object getParentKey() {
        return new RatingPhotoCardsScreen();
    }

    @Override
    public Object createScreenComponent(RatingPhotoCardsScreen.Component parentComponent) {
        return DaggerFindScreen_Component.builder()
                .module(new Module())
                .component(parentComponent)
                .build();
    }

    //region =================== DI ====================

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(FindScreen.class)
        FindPresenter providePresenter() {
            return new FindPresenter();
        }
    }


    @dagger.Component(dependencies = RatingPhotoCardsScreen.Component.class, modules = Module.class)
    @DaggerScope(FindScreen.class)
    public interface Component {
        void inject(FindPresenter presenter);

        void inject(FindView view);

        RootPresenter getRootPresenter();

        RatingPhotoCardModel getRatingPhotoCardModel();
    }


    //region ================ Presenter ================

    public class FindPresenter extends AbstractPresenter<FindView, RatingPhotoCardModel> {

        @Inject
        RefWatcher refWatcher;

        public FindPresenter() {

        }

        @Override
        protected void initActionBar() {
            rootPresenter.createActionbarBuilder()
                    .setVisible(true)
                    .setTab(getView().getViewPager())
                    .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initBottomBar() {
            rootPresenter.showBottomBar(true);
        }

        @Override
        protected void onExitScope() {
            refWatcher.watch(this);
            super.onExitScope();
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().initView();
            }
        }
    }

    //endregion


    //endregion
}
