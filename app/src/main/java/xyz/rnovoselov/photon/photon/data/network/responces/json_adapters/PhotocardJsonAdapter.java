package xyz.rnovoselov.photon.photon.data.network.responces.json_adapters;

import android.util.Log;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import io.realm.internal.android.ISO8601Utils;
import xyz.rnovoselov.photon.photon.data.network.responces.PhotocardRes;

/**
 * Created by novoselov on 15.06.2017.
 */

public class PhotocardJsonAdapter {

    private static final String TAG = PhotocardJsonAdapter.class.getSimpleName();

    @FromJson
    PhotocardRes photocardResFromJson(PhotocardJson json) {
        return new PhotocardRes(
                json.id,
                json.title,
                json.photo,
                json.owner,
                json.filters,
                json.tags,
                json.favorits,
                json.views,
                json.active,
                json.updated != null ? parseToDate(json.updated) : new Date(),
                json.created != null ? parseToDate(json.updated) : new Date());
    }

    @ToJson
    PhotocardJson photocardResToJson(PhotocardRes photocardRes) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        PhotocardJson json = new PhotocardJson();
        json.id = photocardRes.getId();
        json.title = photocardRes.getTitle();
        json.photo = photocardRes.getPhotoUrl();
        json.owner = photocardRes.getOwnerId();
        json.filters = photocardRes.getFilters();
        json.tags = photocardRes.getTags();
        json.favorits = photocardRes.getFavorits();
        json.views = photocardRes.getViews();
        json.active = photocardRes.isActive();
        json.updated = dateFormat.format(photocardRes.getUpdated());
        json.created = dateFormat.format(photocardRes.getCreated());
        return json;
    }

    private Date parseToDate(String date) {
        try {
            return ISO8601Utils.parse(date, new ParsePosition(0));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e(TAG, "Date parsing error");
        }
        return null;
    }
}
