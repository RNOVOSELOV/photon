package xyz.rnovoselov.photon.photon.data.errors;

import xyz.rnovoselov.photon.photon.PhotonApplication;
import xyz.rnovoselov.photon.photon.R;

/**
 * Created by novoselov on 08.06.2017.
 */

public class NetworkApiError extends AbstractPhotonError {

    public NetworkApiError() {
        super(PhotonApplication.getAppContext().getString(R.string.error_api));
    }

    public NetworkApiError(int code) {
        super(PhotonApplication.getAppContext().getString(R.string.error_api_with_code) + String.valueOf(code));
    }
}
