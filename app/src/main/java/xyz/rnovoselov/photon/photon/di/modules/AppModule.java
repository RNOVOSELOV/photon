package xyz.rnovoselov.photon.photon.di.modules;

import android.content.Context;

import com.squareup.leakcanary.RefWatcher;

import dagger.Module;
import dagger.Provides;

/**
 * Created by roman on 02.06.17.
 */

@Module
public class AppModule {

    private Context context;
    private RefWatcher refWatcher;

    public AppModule(Context context, RefWatcher refWatcher) {
        this.context = context;
        this.refWatcher = refWatcher;
    }

    @Provides
    Context provideContext() {
        return context;
    }

    @Provides
    RefWatcher provideCanaryWatcher() {
        return refWatcher;
    }
}
