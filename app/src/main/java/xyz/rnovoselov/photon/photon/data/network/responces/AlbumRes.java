package xyz.rnovoselov.photon.photon.data.network.responces;

import java.util.List;

/**
 * Created by novoselov on 16.06.2017.
 */

public class AlbumRes {

    private String owner;
    private String title;
    private String description;
    private List<PhotocardRes> photocards = null;
    private boolean isFavorite;
    private String id;
    private int views;
    private int favorits;

    public String getOwner() {
        return owner;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public List<PhotocardRes> getPhotocards() {
        return photocards;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public String getId() {
        return id;
    }

    public int getViews() {
        return views;
    }

    public int getFavorits() {
        return favorits;
    }
}
