package xyz.rnovoselov.photon.photon.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by roman on 02.06.17.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface RootScope {
}
