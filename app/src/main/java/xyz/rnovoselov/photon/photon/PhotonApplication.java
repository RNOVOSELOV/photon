package xyz.rnovoselov.photon.photon;

import android.app.Application;
import android.content.Context;
import android.support.v7.app.AppCompatDelegate;

import com.facebook.stetho.Stetho;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import io.realm.Realm;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.di.components.AppComponent;
import xyz.rnovoselov.photon.photon.di.components.DaggerAppComponent;
import xyz.rnovoselov.photon.photon.di.modules.AppModule;
import xyz.rnovoselov.photon.photon.di.modules.RootModule;
import xyz.rnovoselov.photon.photon.mortar.ScreenScoper;
import xyz.rnovoselov.photon.photon.ui.activities.DaggerRootActivity_RootComponent;
import xyz.rnovoselov.photon.photon.ui.activities.RootActivity;

/**
 * Created by roman on 01.06.17.
 */

public class PhotonApplication extends Application {

    private static AppComponent appComponent;
    private static RootActivity.RootComponent rootActivityComponent;

    private RefWatcher refWatcher;

    private MortarScope rootScope;
    private MortarScope rootActivityScope;

    @Override
    public Object getSystemService(String name) {
        if (rootScope != null && rootScope.hasService(name)) {
            return rootScope.getService(name);
        } else {
            return super.getSystemService(name);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        refWatcher = LeakCanary.install(this);

        Realm.init(this);

        createAppComponent();
        createRootActivityComponent();

        // Создаем корневую область видимости
        rootScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME, appComponent)
                .build("Root");

        rootActivityScope = rootScope.buildChild()
                .withService(DaggerService.SERVICE_NAME, rootActivityComponent)
                // BundleServiceRunner  сохраняет презентеры мортара в Bundle, благодаря чему они переживают смену ориентации устройства
                .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                .build(RootActivity.class.getName());

        ScreenScoper.registerScope(rootScope);
        ScreenScoper.registerScope(rootActivityScope);

        Context context = getApplicationContext();
        Stetho.initialize(Stetho.newInitializerBuilder(context)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(context))
                .enableWebKitInspector(RealmInspectorModulesProvider.builder(context).build())
                .build());

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private void createAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext(), refWatcher))
                .build();
    }

    private void createRootActivityComponent() {
        rootActivityComponent = DaggerRootActivity_RootComponent.builder()
                .appComponent(appComponent)
                .rootModule(new RootModule())
                .build();
    }

    public static Context getAppContext() {
        return appComponent.getContext();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static RootActivity.RootComponent getRootActivityComponent() {
        return rootActivityComponent;
    }
}
