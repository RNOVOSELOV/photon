package xyz.rnovoselov.photon.photon.data.network.responces.json_adapters;

import java.util.List;

import xyz.rnovoselov.photon.photon.data.network.responces.PhotocardFiltersRes;

/**
 * Created by novoselov on 08.06.2017.
 */

public class PhotocardJson {
    public String id;
    public String title;
    public String photo;
    public String owner;
    public PhotocardFiltersRes filters;
    public List<String> tags = null;
    public int favorits;
    public int views;
    public boolean active;
    public String updated;
    public String created;
}
