package xyz.rnovoselov.photon.photon.di;

import android.content.Context;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by roman on 02.06.17.
 */

public class DaggerService {

    public static final String SERVICE_NAME = "PHOTON_DAGGER_SERVICE";

    private static Map<Class, Object> componentMap = new HashMap<>();

    @SuppressWarnings("unchecked")
    public static <T> T getDaggerComponent(Context context) {
        //noinspection ResourceType
        return (T) context.getSystemService(SERVICE_NAME);
    }

    public static void registerComponent(Class componentClass, Object daggerComponent) {
        componentMap.put(componentClass, daggerComponent);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public static <T> T getComponent(Class<T> componentClass) {
        Object component = componentMap.get(componentClass);
        return ((T) component);
    }
}
