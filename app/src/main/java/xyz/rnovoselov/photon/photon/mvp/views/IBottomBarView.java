package xyz.rnovoselov.photon.photon.mvp.views;

/**
 * Created by roman on 07.06.17.
 */

public interface IBottomBarView {

    boolean isShown();

    void setBottomBarVisible(boolean isVisible);
}
