package xyz.rnovoselov.photon.photon.mvp.models;

import rx.Observable;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardRealm;

/**
 * Created by roman on 04.07.17.
 */

public class RatingPhotoCardModel extends AbstractModel {

    public Observable<PhotocardRealm> getPhotoCards() {
        return dataProvider.getPhotoCardsFromRealm();
    }

    public void addStringToSearchHistory(String searchString) {
        dataProvider.addSearchHistoryString(searchString);
    }
}
