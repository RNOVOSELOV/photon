package xyz.rnovoselov.photon.photon.ui.screens.find_photocard.detail_filter;

import dagger.Provides;
import mortar.MortarScope;
import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.di.scopes.DaggerScope;
import xyz.rnovoselov.photon.photon.flow.AbstractScreen;
import xyz.rnovoselov.photon.photon.flow.Screen;
import xyz.rnovoselov.photon.photon.mvp.models.RatingPhotoCardModel;
import xyz.rnovoselov.photon.photon.mvp.presenters.AbstractPresenter;
import xyz.rnovoselov.photon.photon.ui.screens.find_photocard.FindScreen;

/**
 * Created by roman on 06.07.17.
 */

@Screen(R.layout.screen_filter)
public class DetailFilterScreen extends AbstractScreen<FindScreen.Component> {

    @Override
    public Object createScreenComponent(FindScreen.Component parentComponent) {
        return DaggerDetailFilterScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    //region ================ DI ================

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(DetailFilterScreen.class)
        DetailFilterPresenter proviceDetailFilterPresenter() {
            return new DetailFilterPresenter();
        }
    }

@dagger.Component(dependencies = FindScreen.Component.class, modules = Module.class)
@DaggerScope(DetailFilterScreen.class)
public interface Component {
    void inject(DetailFilterPresenter presenter);

    void inject(DetailFilterView view);

    RatingPhotoCardModel getRatingPhotoCardModel();
    }

    //endregion

    public class DetailFilterPresenter extends AbstractPresenter<DetailFilterView, RatingPhotoCardModel> {

        @Override
        protected void initActionBar() {

        }

        @Override
        protected void initDagger(MortarScope scope) {

        }

        @Override
        protected void initBottomBar() {

        }
    }
}
