package xyz.rnovoselov.photon.photon.mvp.presenters;

import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import mortar.MortarScope;
import mortar.ViewPresenter;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import xyz.rnovoselov.photon.photon.mvp.models.AbstractModel;
import xyz.rnovoselov.photon.photon.mvp.views.AbstractView;
import xyz.rnovoselov.photon.photon.mvp.views.IRootView;
import xyz.rnovoselov.photon.photon.utils.AppConstants;

/**
 * Created by roman on 03.06.17.
 */

public abstract class AbstractPresenter<V extends AbstractView, M extends AbstractModel> extends ViewPresenter<V> {

    protected final String TAG = AppConstants.TAG_PREFIX + this.getClass().getSimpleName();

    @Inject
    public M model;

    @Inject
    protected RootPresenter rootPresenter;

    protected CompositeSubscription compositeSubscription;

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        initDagger(scope);
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        compositeSubscription = new CompositeSubscription();
        initActionBar();
        initBottomBar();
    }

    @Override
    public void dropView(V view) {
        if (compositeSubscription.hasSubscriptions()) {
            compositeSubscription.unsubscribe();
        }
        super.dropView(view);
    }

    protected abstract void initActionBar();

    protected abstract void initDagger(MortarScope scope);

    protected abstract void initBottomBar();

    @Nullable
    protected IRootView getRootView() {
        return rootPresenter.getRootView();
    }

    protected abstract class ViewSubscriber<T> extends Subscriber<T> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            if (getRootView() != null) {
                getRootView().showError(e);
            }
        }

        @Override
        public abstract void onNext(T t);
    }

    protected <T> Subscription subscribe(Observable<T> observable, ViewSubscriber<T> subscriber) {
        return observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }

}
