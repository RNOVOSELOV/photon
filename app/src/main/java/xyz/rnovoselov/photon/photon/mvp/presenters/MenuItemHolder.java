package xyz.rnovoselov.photon.photon.mvp.presenters;

import android.view.MenuItem;

/**
 * Created by roman on 04.06.17.
 */

public class MenuItemHolder {
    private final CharSequence title;
    private final int iconResId;
    private final MenuItem.OnMenuItemClickListener listener;

    public MenuItemHolder(CharSequence title, MenuItem.OnMenuItemClickListener listener) {
        this.title = title;
        iconResId = 0;
        this.listener = listener;
    }

    public MenuItemHolder(CharSequence title, int iconResId, MenuItem.OnMenuItemClickListener listener) {
        this.title = title;
        this.iconResId = iconResId;
        this.listener = listener;
    }

    public CharSequence getTitle() {
        return title;
    }

    public int getIconResId() {
        return iconResId;
    }

    public MenuItem.OnMenuItemClickListener getListener() {
        return listener;
    }
}
