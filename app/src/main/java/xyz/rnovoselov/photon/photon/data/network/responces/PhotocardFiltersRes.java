package xyz.rnovoselov.photon.photon.data.network.responces;

/**
 * Created by novoselov on 08.06.2017.
 */

public class PhotocardFiltersRes {
    private String dish;
    private String nuances;
    private String decor;
    private String temperature;
    private String light;
    private String lightDirection;
    private String lightSource;

    public String getDish() {
        return dish;
    }

    public String getNuances() {
        return nuances;
    }

    public String getDecor() {
        return decor;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getLight() {
        return light;
    }

    public String getLightDirection() {
        return lightDirection;
    }

    public String getLightSource() {
        return lightSource;
    }
}


