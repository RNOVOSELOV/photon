package xyz.rnovoselov.photon.photon.ui.screens.splash;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;

import flow.Flow;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.mvp.views.AbstractView;

/**
 * Created by roman on 05.06.17.
 */

public class SplashView extends AbstractView<SplashScreen.SplashPresenter> {

    public static final int NORMAL_STATE = 0;
    public static final int DOWNLOAD_STATE = 1;

    private SplashScreen screen;

    public SplashView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            screen = Flow.getKey(this);
            Log.e(TAG, "onLoad()");
        }
    }

    @Override
    protected void afterInflate() {
        super.afterInflate();
        Log.e(TAG, "afterInflate()");
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<SplashScreen.Component>getDaggerComponent(context).inject(this);
    }

    public void setViewState(int state) {
        screen.setSplashViewState(state);
    }

    public int getViewState() {
        return screen.getSplashViewState();
    }

    //region ========================= IView =========================

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    //endregion
}
