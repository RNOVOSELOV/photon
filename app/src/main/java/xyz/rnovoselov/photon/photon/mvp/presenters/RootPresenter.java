package xyz.rnovoselov.photon.photon.mvp.presenters;

import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mortar.MortarScope;
import mortar.Presenter;
import mortar.bundler.BundleService;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.mvp.models.AccountModel;
import xyz.rnovoselov.photon.photon.mvp.views.IRootView;
import xyz.rnovoselov.photon.photon.ui.activities.RootActivity;
import xyz.rnovoselov.photon.photon.utils.AppConfiguration;

/**
 * Created by roman on 02.06.17.
 */

public class RootPresenter extends Presenter<IRootView> {

    private static int TOOLBAR_DEFAULT_MODE = 0;
    private static int TOOLBAR_TAB_MODE = 1;

    @Inject
    AccountModel model;

    private long lastBackPressedTimeInMills;

    public RootPresenter() {
        lastBackPressedTimeInMills = 0;
    }

    @Override
    protected void onEnterScope(MortarScope scope) {
        super.onEnterScope(scope);
        ((RootActivity.RootComponent) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
    }

    @Override
    protected BundleService extractBundleService(IRootView view) {
        return BundleService.getBundleService(((RootActivity) view));
    }

    @Nullable
    public IRootView getRootView() {
        return getView();
    }

    public boolean isDoubleBackClick() {
        long currMills = System.currentTimeMillis();
        if (lastBackPressedTimeInMills + AppConfiguration.MAX_MILLS_TIMEOUT_TO_DOUBLECLICKBACK_EXIT_FROM_APP > currMills) {
            return true;
        }
        lastBackPressedTimeInMills = currMills;
        return false;
    }

    public void showBottomBar(boolean isShow) {
        if (getView() != null) {
            RootActivity activity = (RootActivity) getView();
            activity.setBottomBarVisible(isShow);
        }
    }

    public ActionBarBuilder createActionbarBuilder() {
        return this.new ActionBarBuilder();
    }

    public void clearTollBarMenu() {
        if (getView() != null) {
            RootActivity activity = (RootActivity) getView();
            activity.setMenuItem(null);
            activity.setSubMenuItem(null);
        }
    }

    public class ActionBarBuilder {
        private boolean isGoBack = false;
        private boolean isVisible = true;
        private CharSequence title;
        private List<MenuItemHolder> items = new ArrayList<>();
        private List<MenuItemHolder> itemsSubMenu = new ArrayList<>();
        private ViewPager pager;
        private int toolbarMode = TOOLBAR_DEFAULT_MODE;

        public ActionBarBuilder setBackArrow(boolean enable) {
            this.isGoBack = enable;
            return this;
        }

        public ActionBarBuilder setTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        public ActionBarBuilder setVisible(boolean visible) {
            this.isVisible = visible;
            return this;
        }

        public ActionBarBuilder addAction(MenuItemHolder menuItem) {
            this.items.add(menuItem);
            return this;
        }

        public ActionBarBuilder addActionSubMenu(MenuItemHolder menuItem) {
            this.itemsSubMenu.add(menuItem);
            return this;
        }

        public ActionBarBuilder setTab(ViewPager pager) {
            this.toolbarMode = TOOLBAR_TAB_MODE;
            this.pager = pager;
            return this;
        }

        public void build() {
            if (getView() != null) {
                RootActivity activity = (RootActivity) getView();
                activity.setActionBarVisible(isVisible);
                activity.setActionBarTitle(title);
                activity.setBackArrow(isGoBack);
                activity.setMenuItem(items);
                activity.setSubMenuItem(itemsSubMenu);
                if (toolbarMode == TOOLBAR_TAB_MODE) {
                    activity.setTabLayout(pager);
                } else {
                    activity.removeTabLayout();
                }
            }
        }
    }
}
