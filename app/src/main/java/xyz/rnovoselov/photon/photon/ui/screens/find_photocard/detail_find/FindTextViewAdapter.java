package xyz.rnovoselov.photon.photon.ui.screens.find_photocard.detail_find;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.data.providers.PreferencesProvider;

/**
 * Created by roman on 08.07.17.
 */

class FindTextViewAdapter extends ArrayAdapter implements Filterable {

    private List<String> resultList;
    private Context context;
    private PreferencesProvider preferencesProvider;

    public FindTextViewAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
        this.context = context;
        resultList = new ArrayList<>();
        preferencesProvider = new PreferencesProvider(context);
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return resultList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.autocomplete_find_textview_item, parent, false);
        }
        TextView item = (TextView) convertView.findViewById(R.id.autocomplete_list_item);
        item.setText(getItem(position));
        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                filterResults.count = 0;
                if (constraint != null && constraint.length() > 0) {
                    List<String> stringsHistory = preferencesProvider.getFindList(constraint);
                    if (stringsHistory != null && stringsHistory.size() > 0) {
                        filterResults.count = stringsHistory.size();
                        filterResults.values = stringsHistory;
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<String>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
    }
}
