package xyz.rnovoselov.photon.photon.mvp.models;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import xyz.rnovoselov.photon.photon.data.storage.dto.UserDataDto;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardRealm;
import xyz.rnovoselov.photon.photon.data.storage.realm.UserRealm;

/**
 * Created by roman on 08.06.17.
 */

public class PhotoCardModel extends AbstractModel {

    public PhotoCardModel() {
    }

    public Observable<UserDataDto> getUserObs(String owner) {
        Observable<UserDataDto> userFromRealm = dataProvider.getUserFromRealm(owner)
                .map(UserDataDto::new);

        Observable<UserDataDto> userFromNetwork = dataProvider.getUserById(owner)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(userRes -> dataProvider.saveUserResponseToRealm(owner, userRes))
                .map(userRes -> new UserDataDto(owner, userRes));

        return Observable.mergeDelayError(userFromRealm, userFromNetwork);
    }
}
