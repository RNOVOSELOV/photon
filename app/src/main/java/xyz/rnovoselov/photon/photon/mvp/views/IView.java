package xyz.rnovoselov.photon.photon.mvp.views;

/**
 * Created by roman on 31.05.17.
 */

public interface IView {

    boolean viewOnBackPressed();
}
