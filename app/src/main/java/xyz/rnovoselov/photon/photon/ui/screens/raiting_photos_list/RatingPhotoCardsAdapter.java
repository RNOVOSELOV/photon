package xyz.rnovoselov.photon.photon.ui.screens.raiting_photos_list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardRealm;
import xyz.rnovoselov.photon.photon.ui.custom_views.AspectRatioImageView;

/**
 * Created by roman on 08.06.17.
 */

public class RatingPhotoCardsAdapter extends RecyclerView.Adapter<RatingPhotoCardsAdapter.RatingPhotoCardsViewHolder> {

    private List<PhotocardRealm> photoCards = new ArrayList<>();
    private Context context;
    private RatingPhotoCardsViewHolder.RatingPhotoListener photoListener;

    RatingPhotoCardsAdapter(Context context, RatingPhotoCardsViewHolder.RatingPhotoListener photoListener) {
        this.context = context;
        this.photoListener = photoListener;
    }

    void addPhotoCard(PhotocardRealm photoCard) {
        this.photoCards.add(photoCard);
    }

    void addPhotoCards(List<PhotocardRealm> photoCards) {
        this.photoCards.addAll(photoCards);
    }

    @Override
    public RatingPhotoCardsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_photocards_rating_list, parent, false);
        return new RatingPhotoCardsViewHolder(view, photoListener);
    }

    @Override
    public void onBindViewHolder(RatingPhotoCardsViewHolder holder, int position) {
        final PhotocardRealm photo = photoCards.get(position);
        holder.fillItemView(context, photo);
    }


    @Override
    public int getItemCount() {
        return photoCards.size();
    }

    public PhotocardRealm getPhotoCard(int position) {
        return photoCards.get(position);
    }

    static class RatingPhotoCardsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.photocard_img)
        AspectRatioImageView imageView;
        @BindView(R.id.favorite_txt)
        TextView favoriteTxt;
        @BindView(R.id.views_txt)
        TextView viewsTxt;

        RatingPhotoListener listener;

        RatingPhotoCardsViewHolder(View itemView, RatingPhotoListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.listener = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (listener != null) {
                listener.onItemClick(getAdapterPosition());
            }
        }

        void fillItemView(Context context, PhotocardRealm photo) {
            favoriteTxt.setText(String.valueOf(photo.getFavorits()));
            viewsTxt.setText(String.valueOf(photo.getViews()));
            Glide.with(context)
                    .load(photo.getPhoto())
                    .placeholder(R.drawable.photocard_download_error)
                    .error(R.drawable.photocard_download_error)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .skipMemoryCache(false)
                    .centerCrop()
                    .into(imageView);
        }

        interface RatingPhotoListener {
            void onItemClick(int position);
        }
    }
}
