package xyz.rnovoselov.photon.photon.flow;

import android.util.Log;

import flow.ClassKey;
import xyz.rnovoselov.photon.photon.mortar.ScreenScoper;
import xyz.rnovoselov.photon.photon.utils.AppConstants;

/**
 * Класс абстрактного скрина.
 * В случае, если в унаследованных скринах один и тот же презентер, который принимает входящий параметр,
 * на основании которого создается скрин, то у скринов будет один и тот же скоуп. В данном случае необходимо переопределить
 * методы <b>getScopeName()</b> и <b>unregisterScope()</b>, чтобы имя создаваемых скоупов различалось и в мапу <b>ScreenScoper</b>
 * все корректно клалось.
 */

public abstract class AbstractScreen<T> extends ClassKey {

    protected final String TAG = AppConstants.TAG_PREFIX + this.getClass().getSimpleName();

    public String getScopeName() {
        return getClass().getName();
    }

    public abstract Object createScreenComponent(T parentComponent);

    public void unregisterScope() {
        Log.e(TAG, "UnregisterScope: " + getScopeName());
        ScreenScoper.destroyScreenScope(this.getScopeName());
    }

    public int getLayoutResId() {
        int layout = 0;
        Screen screen;
        screen = this.getClass().getAnnotation(Screen.class);
        if (screen == null) {
            throw new IllegalStateException("@Screen annotation is missing on screen " + getScopeName());
        } else {
            layout = screen.value();
        }
        return layout;
    }
}
