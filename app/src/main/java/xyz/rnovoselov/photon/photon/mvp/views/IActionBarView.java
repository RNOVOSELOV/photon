package xyz.rnovoselov.photon.photon.mvp.views;

import android.support.v4.view.ViewPager;

import java.util.List;

import xyz.rnovoselov.photon.photon.mvp.presenters.MenuItemHolder;

/**
 * Created by roman on 04.06.17.
 */

public interface IActionBarView {

    void setActionBarTitle(CharSequence title);

    void setActionBarVisible(boolean visible);

    void setBackArrow(boolean enabled);

    void setMenuItem(List<MenuItemHolder> items);

    void setSubMenuItem(List<MenuItemHolder> items);

    void setTabLayout(ViewPager pager);

    void removeTabLayout();
}
