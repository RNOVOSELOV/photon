package xyz.rnovoselov.photon.photon.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.leakcanary.RefWatcher;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import xyz.rnovoselov.photon.photon.BuildConfig;
import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.data.errors.AbstractPhotonError;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.di.components.AppComponent;
import xyz.rnovoselov.photon.photon.di.modules.RootModule;
import xyz.rnovoselov.photon.photon.di.scopes.RootScope;
import xyz.rnovoselov.photon.photon.flow.TreeKeyDispatcher;
import xyz.rnovoselov.photon.photon.mvp.models.AccountModel;
import xyz.rnovoselov.photon.photon.mvp.presenters.MenuItemHolder;
import xyz.rnovoselov.photon.photon.mvp.presenters.RootPresenter;
import xyz.rnovoselov.photon.photon.mvp.views.IActionBarView;
import xyz.rnovoselov.photon.photon.mvp.views.IBottomBarView;
import xyz.rnovoselov.photon.photon.mvp.views.IRootView;
import xyz.rnovoselov.photon.photon.mvp.views.IView;
import xyz.rnovoselov.photon.photon.ui.screens.raiting_photos_list.RatingPhotoCardsScreen;
import xyz.rnovoselov.photon.photon.ui.screens.splash.SplashScreen;
import xyz.rnovoselov.photon.photon.utils.AppConstants;

public class RootActivity extends AppCompatActivity implements IRootView, IActionBarView, IBottomBarView {

    private static final String TAG = AppConstants.TAG_PREFIX + RootActivity.class.getSimpleName();

    @BindView(R.id.coordinator)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.root_frame)
    FrameLayout rootFrame;
    @BindView(R.id.progress_view)
    LinearLayout progressLayout;
    @BindView(R.id.appbar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigation;

    @Inject
    RootPresenter rootPresenter;

    private ActionBar actionBar;
    private List<MenuItemHolder> actionBarMenuItems;
    private List<MenuItemHolder> actionBarSubMenuItems;

    //region ================ LIFE CYCLE ================

    @Override
    protected void attachBaseContext(Context newBase) {
        newBase = Flow.configure(newBase, this)
                .defaultKey(new SplashScreen())
                .dispatcher(new TreeKeyDispatcher(this))
                .install();
        super.attachBaseContext(newBase);
    }

    @Override
    public Object getSystemService(@NonNull String name) {
        MortarScope rootActivityScope = MortarScope.findChild(getApplicationContext(), RootActivity.class.getName());
        return rootActivityScope.hasService(name) ? rootActivityScope.getService(name) : super.getSystemService(name);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

        ButterKnife.bind(this);

        RootComponent rootComponent = DaggerService.getDaggerComponent(this);
        rootComponent.inject(this);
        initToolbar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        rootPresenter.takeView(this);
    }

    @Override
    protected void onStop() {
        rootPresenter.dropView(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
//            ScreenScoper.destroyScreenScope(SplashScreen.class.getName());
        }
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (getCurrentScreen() != null && !getCurrentScreen().viewOnBackPressed() && !Flow.get(this).goBack()) {
            closeApplication();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void closeApplication() {
        if (rootPresenter.isDoubleBackClick() || (progressLayout.getVisibility() != View.GONE)) {
            super.onBackPressed();
            ActivityCompat.finishAfterTransition(this);
        } else {
            Snackbar snack = Snackbar.make(coordinatorLayout, "Вы действительно хотите выйти из приложения?", Snackbar.LENGTH_LONG)
                    .setAction("Да", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            RootActivity.super.onBackPressed();
                            ActivityCompat.finishAfterTransition(RootActivity.this);
                        }
                    });
            showSnackbar(snack);
        }
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.ic_custom_gear_black_24dp));
    }

    void showSnackbar(Snackbar snack) {
        if (bottomNavigation.isShown()) {
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) snack.getView().getLayoutParams();

            int[] coordinates = new int[2];
            bottomNavigation.getLocationOnScreen(coordinates);

            params.setMargins(0, 0, 0, bottomNavigation.getHeight());
            snack.getView().setLayoutParams(params);
        }
        View snackBarView = snack.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
        ((TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text)).setTextColor(ContextCompat.getColor(this, R.color.color_primary_dark));
        snack.show();
    }

    //endregion

    //region ================ IRootView =================

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    public void showMessage(String message) {
        Snackbar messageSnack = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
        showSnackbar(messageSnack);
    }

    @Override
    public void showError(Throwable error) {
        if (BuildConfig.DEBUG) {
            showMessage(error.getMessage());
            error.printStackTrace();
        } else if (error instanceof AbstractPhotonError) {
            showMessage(error.getMessage());
        } else {
            showMessage("Извините, что то пошло не так, попробуйте позже.");
            // TODO: 31.05.17 send report to errors collector
        }
    }

    @Override
    public void showProgress(boolean isShow) {
        if (isShow) {
            progressLayout.setVisibility(View.VISIBLE);
        } else {
            progressLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void setSplashBackground(boolean isSplash) {
        if (isSplash) {
            rootFrame.getRootView().setBackground(ContextCompat.getDrawable(this, R.drawable.splash));
        } else {
            rootFrame.getRootView().setBackgroundColor(ContextCompat.getColor(this, R.color.grey_light));
        }
    }

    @Nullable
    @Override
    public IView getCurrentScreen() {
        return (IView) rootFrame.getChildAt(0);
    }

    //endregion

    //region ============== IActionBarView ==============

    @Override
    public void setActionBarTitle(CharSequence title) {
        this.setTitle(title);
    }

    @Override
    public void setActionBarVisible(boolean visible) {
        if (visible) {
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) rootFrame.getLayoutParams();
            TypedValue tv = new TypedValue();
            if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                layoutParams.topMargin = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            }
            rootFrame.setLayoutParams(layoutParams);
            appBarLayout.setVisibility(View.VISIBLE);
        } else {
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) rootFrame.getLayoutParams();
            layoutParams.topMargin = 0;
            rootFrame.setLayoutParams(layoutParams);
            appBarLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void setBackArrow(boolean enabled) {
        if (actionBar != null) {
            if (enabled) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_custom_back_black_24dp));
            } else {
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
        }
    }

    @Override
    public void setMenuItem(List<MenuItemHolder> items) {
        actionBarMenuItems = items;
        supportInvalidateOptionsMenu();
    }

    @Override
    public void setSubMenuItem(List<MenuItemHolder> items) {
        actionBarSubMenuItems = items;
        supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (actionBarMenuItems != null && !actionBarMenuItems.isEmpty()) {
            for (MenuItemHolder itemHolder : actionBarMenuItems) {
                MenuItem item = menu.add(itemHolder.getTitle());
                item.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS)
                        .setIcon(itemHolder.getIconResId())
                        .setOnMenuItemClickListener(itemHolder.getListener());
            }
        }
        if (actionBarSubMenuItems != null && !actionBarSubMenuItems.isEmpty()) {
            for (MenuItemHolder itemHolder : actionBarSubMenuItems) {
                MenuItem item = menu.add(itemHolder.getTitle());
                item.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW)
                        .setIcon(itemHolder.getIconResId())
                        .setOnMenuItemClickListener(itemHolder.getListener());
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTabLayout(ViewPager pager) {
        TabLayout tab = new TabLayout(this);
        tab.setupWithViewPager(pager);
        appBarLayout.addView(tab);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab));
        toolbar.setVisibility(View.GONE);
    }

    @Override
    public void removeTabLayout() {
        toolbar.setVisibility(View.VISIBLE);
        View tab = appBarLayout.getChildAt(1);
        if (tab != null && tab instanceof TabLayout) {
            appBarLayout.removeView(tab);
        }
    }

    //endregion

    //region ============== IBottomBarView ==============

    @Override
    public boolean isShown() {
        return bottomNavigation.getVisibility() == View.VISIBLE;
    }

    @Override
    public void setBottomBarVisible(boolean isVisible) {
        if (isVisible) {
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) rootFrame.getLayoutParams();
            layoutParams.bottomMargin = getResources().getDimensionPixelSize(R.dimen.bottombar_height);
            rootFrame.setLayoutParams(layoutParams);
            bottomNavigation.setVisibility(View.VISIBLE);
        } else {
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) rootFrame.getLayoutParams();
            layoutParams.bottomMargin = 0;
            rootFrame.setLayoutParams(layoutParams);
            bottomNavigation.setVisibility(View.GONE);
        }
        setSplashBackground(!isVisible);
    }

    //endregion

    //region ==================== DI ====================

    @dagger.Component(dependencies = AppComponent.class, modules = RootModule.class)
    @RootScope
    public interface RootComponent {
        void inject(RootActivity activity);

        void inject(RootPresenter model);

        RefWatcher getCanaryWatcher();

        AccountModel getAccountModel();

        RootPresenter getRootPresenter();

        Context getContext();
    }

    //endregion

    //region ================== Events ==================

    @OnClick(R.id.progress_view)
    void onProgressViewClick() {
        // do nothing - this listener cancel listeners of background views, when progress view is shown
    }

    //endregion
}
