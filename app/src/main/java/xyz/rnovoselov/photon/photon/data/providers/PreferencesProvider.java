package xyz.rnovoselov.photon.photon.data.providers;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import xyz.rnovoselov.photon.photon.utils.AppConfiguration;
import xyz.rnovoselov.photon.photon.utils.AppConstants;

/**
 * Created by roman on 01.06.17.
 */

public class PreferencesProvider {

    private SharedPreferences preferences;

    public PreferencesProvider(Context context) {
        preferences = context.getSharedPreferences(AppConstants.APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    /**
     * Сохренение в {@link SharedPreferences} времени последнего обновления карточек на сервере
     *
     * @param lastModified время последнего обновления
     */
    public void saveLastModifiedTime(String lastModified) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(AppConstants.PREFERENCES_LAST_MODIFIED, lastModified);
        editor.apply();
    }

    /**
     * Получение из {@link SharedPreferences} времени последнего обновления карточек на сервере
     *
     * @return время обновления
     */
    public String getLastModifiedTime() {
        return preferences.getString(AppConstants.PREFERENCES_LAST_MODIFIED, AppConstants.DEFAULT_LAST_MODIFIED);
    }

    /**
     * Сохренение в {@link SharedPreferences} значения по которому пользователь искал фотокарточку. Значения сохраняются в {@link Set<String>}.
     *
     * @param value строка, по которой осуществлялся поиск
     */
    public void saveNewFindString(String value) {
        Set<String> set = preferences.getStringSet(AppConstants.PREFERENCES_FIND_LIST, null);
        if (set == null) {
            set = new LinkedHashSet<>();
        }
        set.add(value.toLowerCase());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putStringSet(AppConstants.PREFERENCES_FIND_LIST, set);
        editor.apply();
    }

    /**
     * Получение из {@link SharedPreferences} набора значений типа {@link Set<String>}, который хранит все строковые занчения,
     * по котрым осуществлялся поиск фотокарточек
     *
     * @param constraint
     * @return набор строковых значений
     */
    @Nullable
    public List<String> getFindList(CharSequence constraint) {
        Set<String> stringSet = preferences.getStringSet(AppConstants.PREFERENCES_FIND_LIST, null);
        if (stringSet == null || stringSet.size() <= 0) {
            return null;
        }

        List<String> list = new ArrayList<>();
        for (String val : stringSet) {
            List<String> items = Arrays.asList(val.split("\\s+"));
            for (String str : items) {
                if (str.substring(0, Math.min(str.length(), constraint.length())).equals(constraint.toString().toLowerCase())) {
                    list.add(val);
                    break;
                }
            }
        }
        if (list.size() == 0) {
            return null;
        }
        List<String> sublist = list.subList(Math.max(list.size() - AppConfiguration.MAX_FIND_RESULT, 0), list.size());
        Collections.reverse(sublist);
        return sublist;
    }
}
