package xyz.rnovoselov.photon.photon.data.errors;

import xyz.rnovoselov.photon.photon.PhotonApplication;
import xyz.rnovoselov.photon.photon.R;

/**
 * Created by novoselov on 08.06.2017.
 */

public class NoDownloadedDataError extends AbstractPhotonError {

    public NoDownloadedDataError(String s) {
        super(PhotonApplication.getAppContext().getString(R.string.error_no_data));
    }
}
