package xyz.rnovoselov.photon.photon.ui.screens.photocard;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.fernandocejas.frodo.annotation.RxLogSubscriber;
import com.squareup.leakcanary.RefWatcher;

import javax.inject.Inject;

import dagger.Provides;
import io.realm.RealmChangeListener;
import mortar.MortarScope;
import rx.Subscriber;
import rx.Subscription;
import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.data.storage.dto.PhotoCardDataDto;
import xyz.rnovoselov.photon.photon.data.storage.dto.UserDataDto;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardRealm;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.di.scopes.DaggerScope;
import xyz.rnovoselov.photon.photon.flow.AbstractScreen;
import xyz.rnovoselov.photon.photon.flow.Screen;
import xyz.rnovoselov.photon.photon.mortar.ScreenScoper;
import xyz.rnovoselov.photon.photon.mvp.models.PhotoCardModel;
import xyz.rnovoselov.photon.photon.mvp.presenters.AbstractPresenter;
import xyz.rnovoselov.photon.photon.ui.activities.RootActivity;

/**
 * Created by roman on 14.06.17.
 */

@Screen(R.layout.screen_photocard)
public class PhotoCardScreen extends AbstractScreen<RootActivity.RootComponent> {

    @NonNull
    private PhotoCardDataDto photoCard;

    public PhotoCardScreen(@NonNull PhotoCardDataDto photoCard) {
        this.photoCard = photoCard;
    }

    @Override
    public String getScopeName() {
        return String.format("%s_%s", super.getScopeName(), photoCard.getId());
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerPhotoCardScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    public void unregisterScope() {
        Log.e(TAG, "UnregisterScope: " + getScopeName());
        ScreenScoper.destroyScreenScope(this.getScopeName());
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof PhotoCardScreen) && photoCard.equals(((PhotoCardScreen) o).photoCard);
    }

    @Override
    public int hashCode() {
        return photoCard.hashCode();
    }

    //region =================== DI ====================

    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(PhotoCardScreen.class)
        PhotoCardPresenter providesPhotoCardPresenter() {
            return new PhotoCardPresenter(photoCard);
        }

        @Provides
        @DaggerScope(PhotoCardScreen.class)
        PhotoCardModel providesPhotoCardModel() {
            return new PhotoCardModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @DaggerScope(PhotoCardScreen.class)
    public interface Component {
        void inject(PhotoCardPresenter presenter);

        void inject(PhotoCardView view);
    }

    //endregion


    //region ================ Presenter ================

    public class PhotoCardPresenter extends AbstractPresenter<PhotoCardView, PhotoCardModel> {

        @Inject
        RefWatcher refWatcher;

        Subscription userSubs;

        //private PublishSubject<ActivityResultDto> mActivityResultDtoObs = PublishSubject.create();

        @NonNull
        private PhotoCardDataDto photoCard;

        private RealmChangeListener<PhotocardRealm> cardRealmListener;

        public PhotoCardPresenter(@NonNull PhotoCardDataDto photoCard) {
            this.photoCard = photoCard;
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().showProductView(photoCard);

                cardRealmListener = element -> {
                    if (getView() != null) {
                        getView().showProductView(new PhotoCardDataDto(element));
                    }
                };
//                photoCard.addChangeListener(cardRealmListener);

                userSubs = model.getUserObs(photoCard.getOwner())
                        .subscribe(new UserSubscriber());

                compositeSubscription.add(this.userSubs);
            }
        }

        @Override
        public void dropView(PhotoCardView view) {
//            photoCard.removeChangeListener(cardRealmListener);
            super.dropView(view);
        }

        @Override
        protected void initActionBar() {
            rootPresenter.createActionbarBuilder()
                    .setVisible(true)
                    .setBackArrow(true)
                    .setTitle("Фотокарточка")
                    .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void initBottomBar() {
            rootPresenter.showBottomBar(true);
        }

        @Override
        protected void onExitScope() {
            refWatcher.watch(this);
            super.onExitScope();
        }

        @RxLogSubscriber
        private class UserSubscriber extends Subscriber<UserDataDto> {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(UserDataDto userDataDto) {
                getView().showProductView(userDataDto);
            }
        }
    }

//endregion
}
