package xyz.rnovoselov.photon.photon.ui.custom_views;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.utils.ViewHelper;

/**
 * Created by roman on 04.06.17.
 */

public class AspectRatioImageView extends AppCompatImageView {

    private static final float DEFAULT_ASPECT_RATIO = 1.73f;                // 16:9
    private final float aspectRatio;
    private boolean enableAspect;                                           // отменить применение аспекта (например после анимации)

    public AspectRatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        enableAspect = true;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.AspectRatioImageView);
        aspectRatio = typedArray.getFloat(R.styleable.AspectRatioImageView_aspect_ratio, DEFAULT_ASPECT_RATIO);
        typedArray.recycle();
    }

    /**
     * Метод необхдим для включения/отключения режима автоматческого изменения размера картинки
     *
     * @param enable значение включения/отключения режима автоматического изменения картинки. <b>true</b>-включить режим, <b>false</b>-отключить режим
     */
    public void enableAspectRatio(boolean enable) {
        enableAspect = enable;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (enableAspect) {
            int newWidth = getMeasuredWidth();
            int newHeight = ((int) (newWidth / aspectRatio));
            setMeasuredDimension(newWidth, newHeight);
        }
    }
}
