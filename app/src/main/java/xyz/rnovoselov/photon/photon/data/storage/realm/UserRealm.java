package xyz.rnovoselov.photon.photon.data.storage.realm;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import xyz.rnovoselov.photon.photon.data.network.responces.AlbumRes;
import xyz.rnovoselov.photon.photon.data.network.responces.UserRes;

/**
 * Created by novoselov on 16.06.2017.
 */

public class UserRealm extends RealmObject {

    @PrimaryKey
    private String id;
    private String name;
    private String login;
    private int albumsCount;
    private int photosCount;
    private String avatar;
    private RealmList<AlbumRealm> albums = new RealmList<>();

    public UserRealm() {
    }

    public UserRealm(String userId, UserRes userRes) {
        this.id = userId;
        this.name = userRes.getName();
        this.login = userRes.getLogin();
        this.albumsCount = userRes.getAlbumCount();
        this.photosCount = userRes.getPhotocardCount();
        this.avatar = userRes.getAvatar();

        for (AlbumRes album: userRes.getAlbums()) {
            albums.add(new AlbumRealm(album));
        }
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public int getAlbumsCount() {
        return albumsCount;
    }

    public int getPhotosCount() {
        return photosCount;
    }

    public String getAvatar() {
        return avatar;
    }

    public RealmList<AlbumRealm> getAlbums() {
        return albums;
    }
}
