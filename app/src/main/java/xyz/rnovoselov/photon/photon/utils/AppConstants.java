package xyz.rnovoselov.photon.photon.utils;

/**
 * Created by roman on 23.04.17.
 */

public interface AppConstants {

    String TAG_PREFIX = "PHOTON_";

    String APP_PREFERENCES = "spreferences";
    String PREFERENCES_LAST_MODIFIED = "PREFERENCES_LAST_MODIFIED";
    String PREFERENCES_FIND_LIST = "PREFERENCES_FIND_LIST";

    String DEFAULT_LAST_MODIFIED = "Thu, 01 Jan 1970 00:00:00 GMT";

    String HEADER_IF_MODIFIED_SINCE = "If-Modified-Since";
}
