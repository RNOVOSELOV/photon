package xyz.rnovoselov.photon.photon.data.storage.realm;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import xyz.rnovoselov.photon.photon.data.network.responces.AlbumRes;
import xyz.rnovoselov.photon.photon.data.network.responces.PhotocardRes;

/**
 * Created by novoselov on 16.06.2017.
 */

public class AlbumRealm extends RealmObject {

    @PrimaryKey
    private String id;
    private String owner;
    private String title;
    private String description;
    private RealmList<PhotocardRealm> photocards = new RealmList<>();
    private boolean isFavorite;
    private int views;
    private int favorits;

    public AlbumRealm() {
    }

    public AlbumRealm(AlbumRes album) {
        id = album.getId();
        owner = album.getOwner();
        title = album.getTitle();
        description = album.getDescription();
        isFavorite = album.isFavorite();
        views = album.getViews();
        favorits = album.getFavorits();

        for (PhotocardRes photocardRes: album.getPhotocards()) {
            photocards.add(new PhotocardRealm(photocardRes));
        }
    }
}
