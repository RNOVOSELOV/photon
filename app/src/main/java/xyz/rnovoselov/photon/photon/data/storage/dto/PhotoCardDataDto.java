package xyz.rnovoselov.photon.photon.data.storage.dto;

import java.util.ArrayList;
import java.util.List;

import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardRealm;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardTagRealm;

/**
 * Created by novoselov on 09.06.2017.
 */

public class PhotoCardDataDto {

    private String id;
    private int favorits;
    private int views;
    private String photo;
    private String title;
    private String owner;
    List<String> tags = new ArrayList<>();

    public PhotoCardDataDto(PhotocardRealm photocardRealm) {
        this.id = photocardRealm.getId();
        this.favorits = photocardRealm.getFavorits();
        this.views = photocardRealm.getViews();
        this.photo = photocardRealm.getPhoto();
        this.title = photocardRealm.getTitle();
        this.owner = photocardRealm.getOwner();

        for (PhotocardTagRealm tag : photocardRealm.getTags()) {
            tags.add(tag.getTag());
        }
    }

    public String getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public int getFavorits() {
        return favorits;
    }

    public int getViews() {
        return views;
    }

    public String getPhoto() {
        return photo;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getTags() {
        return tags;
    }
}
