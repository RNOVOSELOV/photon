package xyz.rnovoselov.photon.photon.di.components;

import javax.inject.Singleton;

import dagger.Component;
import xyz.rnovoselov.photon.photon.data.providers.DataProvider;
import xyz.rnovoselov.photon.photon.di.modules.LocalModule;
import xyz.rnovoselov.photon.photon.di.modules.NetworkModule;

/**
 * Created by roman on 02.06.17.
 */

@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataProviderComponent {
    void inject(DataProvider dataProvider);
}
