package xyz.rnovoselov.photon.photon.data.network.responces;

import java.util.Date;
import java.util.List;

/**
 * Created by novoselov on 08.06.2017.
 */

public class PhotocardRes {
    private String id;
    private String title;
    private String photo;
    private String owner;
    private PhotocardFiltersRes filters;
    private List<String> tags = null;
    private int favorits;
    private int views;
    private boolean active;
    private Date updated;
    private Date created;

    public PhotocardRes(String id, String title, String photo, String owner, PhotocardFiltersRes filters, List<String> tags, int favorits, int views, boolean active, Date updated, Date created) {
        this.id = id;
        this.title = title;
        this.photo = photo;
        this.owner = owner;
        this.filters = filters;
        this.tags = tags;
        this.favorits = favorits;
        this.views = views;
        this.active = active;
        this.updated = updated;
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPhotoUrl() {
        return photo;
    }

    public String getOwnerId() {
        return owner;
    }

    public PhotocardFiltersRes getFilters() {
        return filters;
    }

    public List<String> getTags() {
        return tags;
    }

    public int getFavorits() {
        return favorits;
    }

    public int getViews() {
        return views;
    }

    public boolean isActive() {
        return active;
    }

    public Date getUpdated() {
        return updated;
    }

    public Date getCreated() {
        return created;
    }
}
