package xyz.rnovoselov.photon.photon.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import rx.Observable;
import xyz.rnovoselov.photon.photon.PhotonApplication;

/**
 * Created by roman on 01.06.17.
 */

public class NetworkStatusChecker {

    private static Boolean isNetworkAvailable() {
        ConnectivityManager cm = ((ConnectivityManager) PhotonApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE));
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public static Observable<Boolean> isInternetAvailable() {
        return Observable.just(isNetworkAvailable());
    }
}
