package xyz.rnovoselov.photon.photon.mvp.views;

import android.support.annotation.Nullable;

/**
 * Created by roman on 31.05.17.
 */

public interface IRootView extends IView {

    void showMessage(String message);

    void showError(Throwable error);

    void showProgress(boolean isShow);

    void setSplashBackground(boolean isSplash);

    void closeApplication();

    @Nullable
    IView getCurrentScreen();
}
