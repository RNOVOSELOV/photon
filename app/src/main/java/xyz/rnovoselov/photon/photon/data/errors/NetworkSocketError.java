package xyz.rnovoselov.photon.photon.data.errors;

import xyz.rnovoselov.photon.photon.PhotonApplication;
import xyz.rnovoselov.photon.photon.R;

/**
 * Created by roman on 09.06.17.
 */

public class NetworkSocketError extends AbstractPhotonError {

    public NetworkSocketError() {
        super(PhotonApplication.getAppContext().getString(R.string.error_socket));
    }
}
