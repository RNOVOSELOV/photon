package xyz.rnovoselov.photon.photon.mvp.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import javax.inject.Inject;

import butterknife.ButterKnife;
import xyz.rnovoselov.photon.photon.flow.AbstractScreen;
import xyz.rnovoselov.photon.photon.mvp.presenters.AbstractPresenter;
import xyz.rnovoselov.photon.photon.utils.AppConstants;

/**
 * Created by roman on 03.06.17.
 */

public abstract class AbstractView<P extends AbstractPresenter> extends FrameLayout implements IView {

    protected final String TAG = AppConstants.TAG_PREFIX + this.getClass().getSimpleName();

    @Inject
    protected P presenter;

    public AbstractView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            initDagger(context);
        }
    }

    protected abstract void initDagger(Context context);

    /**
     * Метод, необходимый для выполнения действий,
     * которые необходимо совершить по окончании инфлэйта вью
     */
    protected void afterInflate() {

    }

    /**
     * Метод, необходимый для выполнения действий,
     * которые необходимо выполнить перед дропом вью
     */
    protected void beforeDrop() {

    }

    /**
     * Метод для запуска анимации (выполняется после инфлэйта вью)
     */
    protected void startInitAnimation() {

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        if (!isInEditMode()) {
            afterInflate();
            startInitAnimation();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            presenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            beforeDrop();
            presenter.dropView(this);
        }
    }
}
