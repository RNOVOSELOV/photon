package xyz.rnovoselov.photon.photon.ui.screens.find_photocard.detail_find;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.ImageButton;

import butterknife.BindView;
import butterknife.OnClick;
import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.mvp.views.AbstractView;

/**
 * Created by roman on 05.07.17.
 */

public class DetailFindView extends AbstractView<DetailFindScreen.DetailFindPresenter> {

    @BindView(R.id.search_tv)
    AppCompatAutoCompleteTextView searchAutocompleteTextView;
    @BindView(R.id.search_action_ibutton)
    ImageButton searchBtn;

    public DetailFindView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void afterInflate() {
        super.afterInflate();
        int iconSize = (int) getResources().getDimension(R.dimen.icons_size);
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_custom_back_arrow_black_24dp);
        drawable.setBounds(0, 0, iconSize * 2, 0); // that is the trick!
        searchAutocompleteTextView.setCompoundDrawables(null, null, drawable, null);
        searchAutocompleteTextView.setThreshold(1);
        searchAutocompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int drawableRes;
                if (s.length() <= 0) {
                    drawableRes = R.drawable.ic_custom_back_arrow_black_24dp;
                } else {
                    drawableRes = R.drawable.ic_check_black_24dp;
                }
                searchBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), drawableRes));
            }
        });
        searchAutocompleteTextView.setAdapter(new FindTextViewAdapter(getContext(), R.layout.autocomplete_find_textview_item));
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<DetailFindScreen.Component>getDaggerComponent(context).inject(this);
    }

    @OnClick(R.id.clear_action_ib)
    void onClearClick() {
        searchAutocompleteTextView.setText("");
    }

    @OnClick(R.id.search_action_ibutton)
    void onSearchClick() {
        presenter.searchPhotos(searchAutocompleteTextView.getText().toString());
    }
}
