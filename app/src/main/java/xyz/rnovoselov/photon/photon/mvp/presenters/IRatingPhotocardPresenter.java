package xyz.rnovoselov.photon.photon.mvp.presenters;

import xyz.rnovoselov.photon.photon.data.storage.dto.PhotoCardDataDto;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardRealm;

/**
 * Created by roman on 08.06.17.
 */

public interface IRatingPhotocardPresenter {

    void clickOnPhotoCard(PhotocardRealm position);
}
