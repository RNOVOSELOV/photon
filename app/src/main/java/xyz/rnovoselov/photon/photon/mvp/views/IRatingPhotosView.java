package xyz.rnovoselov.photon.photon.mvp.views;

/**
 * Created by novoselov on 09.06.2017.
 */

public interface IRatingPhotosView {

    void showRatingPhotosView();
}
