package xyz.rnovoselov.photon.photon.ui.custom_views;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.widget.TextView;

import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.utils.ViewHelper;

/**
 * Created by roman on 15.06.17.
 */

public class TagTextViewBuilder {

    private Context context;
    private String text;
    private int borderColor;
    private float textSize;
    private int padding;

    public TagTextViewBuilder(Context context) {
        this.context = context;
        this.borderColor = ContextCompat.getColor(context, android.R.color.black);

        this.text = "";
        textSize = 12;
        padding = (int) ViewHelper.convertDpToPixel(10, context);
    }

    public TagTextViewBuilder setText(String text) {
        this.text = text;
        return this;
    }

    public TagTextViewBuilder setTextSize(float size) {
        this.textSize = size;
        return this;
    }

    public TagTextViewBuilder setPadding(int padding) {
        this.padding = (int) ViewHelper.convertDpToPixel(padding, context);
        return this;
    }

    public TagTextViewBuilder setBackgroundBorderColor(int color) {
        this.borderColor = color;
        return this;
    }

    public TextView build() {

        TextView tv = new TextView(context);
        tv.setText(text);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        tv.setPadding(padding, padding, padding, padding);

        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadius(8);
        shape.setColor(ContextCompat.getColor(context, R.color.color_transparent));
        shape.setStroke(2, borderColor);
        tv.setBackground(shape);


        context = null;
        return tv;
    }
}
