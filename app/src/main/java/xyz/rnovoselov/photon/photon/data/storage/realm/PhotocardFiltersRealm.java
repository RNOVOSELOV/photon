package xyz.rnovoselov.photon.photon.data.storage.realm;

import io.realm.RealmObject;
import xyz.rnovoselov.photon.photon.data.network.responces.PhotocardFiltersRes;

/**
 * Created by novoselov on 08.06.2017.
 */

public class PhotocardFiltersRealm extends RealmObject {

    private String dish;
    private String nuances;
    private String decor;
    private String temperature;
    private String light;
    private String lightDirection;
    private String lightSource;

    public PhotocardFiltersRealm() {
    }

    public PhotocardFiltersRealm(PhotocardFiltersRes filtersRes) {

        this.dish = filtersRes.getDish();
        this.nuances = filtersRes.getNuances();
        this.decor = filtersRes.getDecor();
        this.temperature = filtersRes.getTemperature();
        this.light = filtersRes.getLight();
        this.lightDirection = filtersRes.getLightDirection();
        this.lightSource = filtersRes.getLightSource();
    }
}
