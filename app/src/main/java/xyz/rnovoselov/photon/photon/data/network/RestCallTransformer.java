package xyz.rnovoselov.photon.photon.data.network;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import retrofit2.Response;
import rx.Observable;
import xyz.rnovoselov.photon.photon.data.errors.NetworkApiError;

/**
 * Created by roman on 01.06.17.
 */

public class RestCallTransformer<R> implements Observable.Transformer<Response<R>, R> {

    @Override
    @RxLogObservable
    public Observable<R> call(Observable<Response<R>> responseObservable) {
        return responseObservable.flatMap(rResponse -> {
            switch (rResponse.code()) {
                case 200:
                    return Observable.just(rResponse.body());
                case 304:
                    return Observable.empty();
                default:
                    return Observable.error(new NetworkApiError(rResponse.code()));
            }
        });
    }
}
