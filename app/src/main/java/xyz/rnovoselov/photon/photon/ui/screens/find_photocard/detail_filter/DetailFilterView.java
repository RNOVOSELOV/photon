package xyz.rnovoselov.photon.photon.ui.screens.find_photocard.detail_filter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.mvp.views.AbstractView;

/**
 * Created by roman on 06.07.17.
 */

public class DetailFilterView extends AbstractView<DetailFilterScreen.DetailFilterPresenter> {

    public DetailFilterView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<DetailFilterScreen.Component>getDaggerComponent(context).inject(this);
    }
}
