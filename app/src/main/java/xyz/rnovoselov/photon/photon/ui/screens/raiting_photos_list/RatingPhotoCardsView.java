package xyz.rnovoselov.photon.photon.ui.screens.raiting_photos_list;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;

import butterknife.BindView;
import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.mvp.views.AbstractView;
import xyz.rnovoselov.photon.photon.mvp.views.IRatingPhotosView;

/**
 * Created by roman on 08.06.17.
 */

public class RatingPhotoCardsView extends AbstractView<RatingPhotoCardsScreen.RatingPhotoCardsPresenter> implements IRatingPhotosView {

    @BindView(R.id.photos_recycler)
    RecyclerView recyclerView;

    private RatingPhotoCardsAdapter adapter;

    public RatingPhotoCardsView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        presenter.processBackPressed();
        return true;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<RatingPhotoCardsScreen.Component>getDaggerComponent(context).inject(this);
        adapter = new RatingPhotoCardsAdapter(context, new RatingPhotoCardsAdapter.RatingPhotoCardsViewHolder.RatingPhotoListener() {
            @Override
            public void onItemClick(int position) {
                presenter.clickOnPhotoCard(adapter.getPhotoCard(position));
            }
        });
    }

    @Override
    protected void afterInflate() {
        super.afterInflate();
        /*
        int columnCount;
        if (getContext().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            columnCount = 2;
        } else {
            columnCount = 4;
        }
        */
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), calculateCountOfRvColumns());
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(layoutManager);
    }

    public RatingPhotoCardsAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void showRatingPhotosView() {
        recyclerView.swapAdapter(adapter, false);
    }

    public int getCurrentRecyclerPosition() {
        return 0;
    }

    private int calculateCountOfRvColumns() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float width = displayMetrics.widthPixels / displayMetrics.density;
        int count = (int) (width / getResources().getDimension(R.dimen.approx_rating_card_width));
        if (count <= 1) {
            return 2;
        }
        return count;
    }
}
