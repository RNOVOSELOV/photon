package xyz.rnovoselov.photon.photon.mvp.views;

import xyz.rnovoselov.photon.photon.data.storage.dto.PhotoCardDataDto;
import xyz.rnovoselov.photon.photon.data.storage.dto.UserDataDto;

/**
 * Created by novoselov on 15.06.2017.
 */

public interface IPhotoCardView {

    void showProductView(PhotoCardDataDto photoCard);

    void showProductView(UserDataDto userData);
}
