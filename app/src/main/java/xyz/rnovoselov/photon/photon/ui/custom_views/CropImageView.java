package xyz.rnovoselov.photon.photon.ui.custom_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import xyz.rnovoselov.photon.photon.R;

/**
 * Created by novoselov on 07.06.2017.
 */

public class CropImageView extends AppCompatImageView {

    float widthFraction = 0, heightFraction = 0;

    public CropImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CropImageView);
        widthFraction = typedArray.getFloat(R.styleable.CropImageView_width_fraction_start, 0);
        heightFraction = typedArray.getFloat(R.styleable.CropImageView_height_fraction_start, 0);
        typedArray.recycle();
    }

    private void setup() {
        setScaleType(ScaleType.MATRIX);
    }

    /**
     * Метод устанавливает как далеко вправо и вниз помещается верхний левый угол обрезки в отрезке от 0 до 1.
     *
     * @param widthFraction  по ширине
     * @param heightFraction по высоте
     */
    public void setOffset(float widthFraction, float heightFraction) {
        this.widthFraction = widthFraction;
        this.heightFraction = heightFraction;
    }


    @Override
    protected boolean setFrame(int l, int t, int r, int b) {
        if (getDrawable() == null)
            return super.setFrame(l, t, r, b);


        Matrix matrix = getImageMatrix();

        float scale;
        int viewWidth = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
        int viewHeight = getMeasuredHeight() - getPaddingTop() - getPaddingBottom();
        int drawableWidth = getDrawable().getIntrinsicWidth();
        int drawableHeight = getDrawable().getIntrinsicHeight();

        // Get the scale.
        if (drawableWidth * viewHeight > drawableHeight * viewWidth) {
            // Drawable is flatter than view. Scale it to fill the view height.
            // A Top/Bottom crop here should be identical in this case.
            scale = (float) viewHeight / (float) drawableHeight;
        } else {
            // Drawable is taller than view. Scale it to fill the view width.
            // Left/Right crop here should be identical in this case.
            scale = (float) viewWidth / (float) drawableWidth;
        }

        float viewToDrawableWidth = viewWidth / scale;
        float viewToDrawableHeight = viewHeight / scale;
        float xOffset = widthFraction * (drawableWidth - viewToDrawableWidth);
        float yOffset = heightFraction * (drawableHeight - viewToDrawableHeight);

        // Define the rect from which to take the image portion.
        RectF drawableRect = new RectF(xOffset, yOffset, xOffset + viewToDrawableWidth,
                yOffset + viewToDrawableHeight);
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        matrix.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.FILL);

        setImageMatrix(matrix);

        return super.setFrame(l, t, r, b);
    }
}


