package xyz.rnovoselov.photon.photon.mvp.presenters;

/**
 * Created by novoselov on 07.06.2017.
 */

public interface ISplashPresenter {

    void downloadBestRatingPhotos();

    void downloadTags();

    void downloadUserAlbums();

    void showProgress();

    void hideProgress ();

    void goToRaitingScreen();
}
