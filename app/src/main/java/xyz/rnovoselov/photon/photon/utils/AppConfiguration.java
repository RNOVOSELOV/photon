package xyz.rnovoselov.photon.photon.utils;

/**
 * Created by roman on 01.05.17.
 */

public interface AppConfiguration {

    String BASE_URL = "http://207.154.248.163:5000/";
    String DEFAULT_LAST_UPDATE_DATE = "Thu, 01 Jan 1970 00:00:00 GMT";

    int MAX_OKHTTP_CONNECTION_TIMEOUT = 5000;
    int MAX_OKHTTP_READ_TIMEOUT = 5000;
    int MAX_OKHTTP_WRITE_TIMEOUT = 5000;

    int MAX_MILLS_TIMEOUT_TO_DOUBLECLICKBACK_EXIT_FROM_APP = 2000;
    int PHOTOCARD_COUNT_PER_QUERY = 60;

    int MAX_FIND_RESULT = 5;
}
