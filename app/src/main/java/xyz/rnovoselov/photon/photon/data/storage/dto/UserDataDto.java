package xyz.rnovoselov.photon.photon.data.storage.dto;

import xyz.rnovoselov.photon.photon.data.network.responces.AlbumRes;
import xyz.rnovoselov.photon.photon.data.network.responces.UserRes;
import xyz.rnovoselov.photon.photon.data.storage.realm.UserRealm;

/**
 * Created by novoselov on 16.06.2017.
 */

public class UserDataDto {
    private String id;
    private String name;
    private String login;
    private int albumsCount;
    private int photosCount;
    private String avatar;

    public UserDataDto(String id, String name, String login, int albumsCount, int photosCount, String avatar) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.albumsCount = albumsCount;
        this.photosCount = photosCount;
        this.avatar = avatar;
    }

    public UserDataDto(String userId, UserRes res) {
        this.id = userId;
        this.name = res.getName();
        this.login = res.getLogin();
        this.albumsCount = res.getAlbumCount();
        this.photosCount = res.getPhotocardCount();
        this.avatar = res.getAvatar();
    }

    public UserDataDto(UserRealm element) {
        this.id = element.getId();
        this.name = element.getName();
        this.login = element.getLogin();
        this.albumsCount = element.getAlbumsCount();
        this.photosCount = element.getPhotosCount();
        this.avatar = element.getAvatar();
    }

    public UserDataDto(UserRes userRes) {
        this.name = userRes.getName();
        this.login = userRes.getLogin();
        this.avatar = userRes.getAvatar();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public int getAlbumsCount() {
        return albumsCount;
    }

    public int getPhotosCount() {
        return photosCount;
    }

    public String getAvatar() {
        return avatar;
    }
}
