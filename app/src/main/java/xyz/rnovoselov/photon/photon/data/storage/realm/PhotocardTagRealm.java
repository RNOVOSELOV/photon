package xyz.rnovoselov.photon.photon.data.storage.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by novoselov on 08.06.2017.
 */

public class PhotocardTagRealm extends RealmObject {

    @PrimaryKey
    private String tag;

    public PhotocardTagRealm() {
    }

    public PhotocardTagRealm(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}
