package xyz.rnovoselov.photon.photon.data.providers;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import xyz.rnovoselov.photon.photon.PhotonApplication;
import xyz.rnovoselov.photon.photon.data.errors.NetworkAvailableError;
import xyz.rnovoselov.photon.photon.data.network.RestCallTransformer;
import xyz.rnovoselov.photon.photon.data.network.RestService;
import xyz.rnovoselov.photon.photon.data.network.responces.PhotocardRes;
import xyz.rnovoselov.photon.photon.data.network.responces.UserRes;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardRealm;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardTagRealm;
import xyz.rnovoselov.photon.photon.data.storage.realm.UserRealm;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.di.components.DaggerDataProviderComponent;
import xyz.rnovoselov.photon.photon.di.components.DataProviderComponent;
import xyz.rnovoselov.photon.photon.di.modules.LocalModule;
import xyz.rnovoselov.photon.photon.di.modules.NetworkModule;
import xyz.rnovoselov.photon.photon.utils.AppConstants;
import xyz.rnovoselov.photon.photon.utils.NetworkStatusChecker;

/**
 * Created by roman on 01.06.17.
 */

public class DataProvider {

    private static final String TAG = AppConstants.TAG_PREFIX + DataProvider.class.getSimpleName();

    @Inject
    PreferencesProvider preferencesProvider;
    @Inject
    RestService restService;
    @Inject
    RealmProvider realmProvider;

    public void addSearchHistoryString(String searchString) {
        preferencesProvider.saveNewFindString(searchString);
    }

    private static class DataProviderHolder {
        private final static DataProvider instance = new DataProvider();
    }

    public static DataProvider getInstance() {
        return DataProviderHolder.instance;
    }

    private DataProvider() {
        DataProviderComponent component = DaggerService.getComponent(DataProviderComponent.class);
        if (component == null) {
            component = DaggerDataProviderComponent.builder()
                    .appComponent(PhotonApplication.getAppComponent())
                    .localModule(new LocalModule())
                    .networkModule(new NetworkModule())
                    .build();
            DaggerService.registerComponent(DataProviderComponent.class, component);
        }
        component.inject(this);
    }

    //region ========================== REST ===========================

    public Observable<PhotocardRes> getPhotoCardsFromNetwork(int maxPhotoCount, int offset) {
        return NetworkStatusChecker.isInternetAvailable()
                .flatMap(aBoolean -> aBoolean ? restService.getListPhotos(AppConstants.DEFAULT_LAST_MODIFIED) : Observable.error(new NetworkAvailableError()))
                .compose(new RestCallTransformer<>())
                .subscribeOn(Schedulers.newThread())
                .flatMap(Observable::from);
    }

    public Observable<List<PhotocardTagRealm>> getAllTagsFromNetworkAndSaveToRealm() {
        return NetworkStatusChecker.isInternetAvailable()
                .flatMap(aBoolean -> aBoolean ? restService.getTags() : Observable.error(new NetworkAvailableError()))
                .compose(new RestCallTransformer<>())
                .flatMap(Observable::from)
                .map(PhotocardTagRealm::new)
                .toList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(this::savePhotocardTagsToRealm)
                .flatMap(photocardTagRealms -> Observable.empty());
    }

    public Observable<UserRes> getUserById(String userId) {
        return NetworkStatusChecker.isInternetAvailable()
                .flatMap(aBoolean -> aBoolean ? restService.getUser(AppConstants.DEFAULT_LAST_MODIFIED, userId) : Observable.error(new NetworkAvailableError()))
                .compose(new RestCallTransformer<>());
                //.doOnNext(userRes -> realmProvider.saveUser(userId, userRes));
    }

    //endregion

    //region ========================== REALM ==========================


    public Observable<PhotocardRes> savePhotoCardResponseToRealm(Observable<PhotocardRes> photoCardResObs) {
        return photoCardResObs
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(this::savePhotoCardResponseToRealm)
                .flatMap(photocardRes -> Observable.empty());
    }

    public void savePhotoCardResponseToRealm(PhotocardRes photocardRes) {
        realmProvider.savePhotocardResponseToRealm(photocardRes);
    }

    public void savePhotocardTagsToRealm(List<PhotocardTagRealm> tags) {
        realmProvider.saveTagsToRealm(tags);
    }

    public Observable<PhotocardRealm> getPhotoCardsFromRealm() {
        return realmProvider.getPhotoCards();
    }

    public void saveUserResponseToRealm(String id, UserRes userRes) {
        realmProvider.saveUser(id, userRes);
    }

    public Observable<UserRealm> getUserFromRealm(String userId) {
        return realmProvider.getUser(userId);
    }

    //endregion
}
