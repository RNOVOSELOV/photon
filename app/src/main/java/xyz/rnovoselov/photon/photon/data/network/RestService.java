package xyz.rnovoselov.photon.photon.data.network;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;
import xyz.rnovoselov.photon.photon.data.network.responces.PhotocardRes;
import xyz.rnovoselov.photon.photon.data.network.responces.UserRes;
import xyz.rnovoselov.photon.photon.utils.AppConstants;

/**
 * Created by roman on 02.06.17.
 */

public interface RestService {

    @GET("photocard/list")
    Observable<Response<List<PhotocardRes>>> getListPhotos(@Header(AppConstants.HEADER_IF_MODIFIED_SINCE) String lastUpdateCardsDate);

    @GET("photocard/list")
    Observable<Response<List<PhotocardRes>>> getListPhotos(@Header(AppConstants.HEADER_IF_MODIFIED_SINCE) String lastUpdateCardsDate, @Query("limit") int maxPhotoCount, @Query("offset") int offset);

    @GET("photocard/tags")
    Observable<Response<List<String>>> getTags();

    @GET("user/{userId}")
    Observable<Response<UserRes>> getUser(@Header(AppConstants.HEADER_IF_MODIFIED_SINCE) String lastUpdateUserDate, @Path("userId") String userId);
}
