package xyz.rnovoselov.photon.photon.data.errors;

import xyz.rnovoselov.photon.photon.PhotonApplication;
import xyz.rnovoselov.photon.photon.R;

/**
 * Created by novoselov on 08.06.2017.
 */

public class NetworkAvailableError extends AbstractPhotonError {

    public NetworkAvailableError() {
        super(PhotonApplication.getAppContext().getString(R.string.error_network));
    }
}
