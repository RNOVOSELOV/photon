package xyz.rnovoselov.photon.photon.mvp.models;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import xyz.rnovoselov.photon.photon.data.network.responces.PhotocardRes;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardRealm;
import xyz.rnovoselov.photon.photon.data.storage.realm.PhotocardTagRealm;
import xyz.rnovoselov.photon.photon.utils.AppConfiguration;

/**
 * Created by roman on 05.06.17.
 */

public class SplashModel extends AbstractModel {

    public Observable updatePhotoCardsList() {
        return dataProvider.savePhotoCardResponseToRealm(dataProvider.getPhotoCardsFromNetwork(AppConfiguration.PHOTOCARD_COUNT_PER_QUERY, 0));
    }

    public Observable updatePhotoCardsTags() {
        return dataProvider.getAllTagsFromNetworkAndSaveToRealm();
    }
}
