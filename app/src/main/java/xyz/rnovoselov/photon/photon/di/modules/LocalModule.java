package xyz.rnovoselov.photon.photon.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import xyz.rnovoselov.photon.photon.data.providers.PreferencesProvider;
import xyz.rnovoselov.photon.photon.data.providers.RealmProvider;

/**
 * Created by roman on 02.06.17.
 */

@Module
public class LocalModule {

    @Provides
    @Singleton
    PreferencesProvider providePreferences(Context context) {
        return new PreferencesProvider(context);
    }

    @Provides
    @Singleton
    RealmProvider provideRealm(Context context) {
        return new RealmProvider();
    }
}
