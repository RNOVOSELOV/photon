package xyz.rnovoselov.photon.photon.ui.screens.photocard;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.flexbox.FlexboxLayout;

import butterknife.BindView;
import xyz.rnovoselov.photon.photon.R;
import xyz.rnovoselov.photon.photon.data.storage.dto.PhotoCardDataDto;
import xyz.rnovoselov.photon.photon.data.storage.dto.UserDataDto;
import xyz.rnovoselov.photon.photon.di.DaggerService;
import xyz.rnovoselov.photon.photon.mvp.views.AbstractView;
import xyz.rnovoselov.photon.photon.mvp.views.IPhotoCardView;
import xyz.rnovoselov.photon.photon.ui.custom_views.AspectRatioImageView;
import xyz.rnovoselov.photon.photon.ui.custom_views.TagTextViewBuilder;
import xyz.rnovoselov.photon.photon.utils.ViewHelper;

/**
 * Created by roman on 14.06.17.
 */

public class PhotoCardView extends AbstractView<PhotoCardScreen.PhotoCardPresenter> implements IPhotoCardView {

    @BindView(R.id.photocard_img)
    AspectRatioImageView photocardImage;
    @BindView(R.id.photocard_title)
    TextView photocardTitle;
    @BindView(R.id.author_avatar)
    ImageView avatarImage;
    @BindView(R.id.author_name)
    TextView authorName;
    @BindView(R.id.author_albums_count)
    TextView albumsCount;
    @BindView(R.id.author_cards_count)
    TextView cardsCount;
    @BindView(R.id.tag_container)
    FlexboxLayout tagContainer;

    public PhotoCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<PhotoCardScreen.Component>getDaggerComponent(context).inject(this);
    }

    @Override
    public void showProductView(final PhotoCardDataDto photoCardDataDto) {
        photocardTitle.setText(photoCardDataDto.getTitle());
//        authorName.setText(photoCardDataDto.getOwner());
        for (String tag : photoCardDataDto.getTags()) {
            TextView tv = new TagTextViewBuilder(getContext())
                    .setText("#" + tag)
                    .setPadding(10)
                    .setTextSize(13)
                    .build();
            tagContainer.addView(tv);

            FlexboxLayout.LayoutParams layoutParams = (FlexboxLayout.LayoutParams) tv.getLayoutParams();
            layoutParams.setMargins(0, 0, (int) ViewHelper.convertDpToPixel(16, getContext()), (int) ViewHelper.convertDpToPixel(16, getContext()));
            tv.setLayoutParams(layoutParams);
        }

        Glide.with(getContext())
                .load(photoCardDataDto.getPhoto())
                .placeholder(R.drawable.photocard_download_error)
                .error(R.drawable.photocard_download_error)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .skipMemoryCache(false)
                .centerCrop()
                .into(photocardImage);
    }

    @Override
    public void showProductView(UserDataDto userData) {
        authorName.setText(userData.getName());
        cardsCount.setText(String.valueOf(userData.getPhotosCount()));
        albumsCount.setText(String.valueOf(userData.getAlbumsCount()));

        Glide.with(getContext())
                .load(userData.getAvatar())
                .placeholder(R.drawable.photocard_download_error)
                .error(R.drawable.photocard_download_error)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .skipMemoryCache(false)
                .centerCrop()
                .into(avatarImage);
    }
}
